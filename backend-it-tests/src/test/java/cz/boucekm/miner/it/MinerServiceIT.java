package cz.boucekm.miner.it;

import cz.boucekm.miner.it.support.RestService;
import net.sf.lightair.LightAir;
import net.sf.lightair.annotation.Setup;
import net.sf.lightair.annotation.Verify;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static cz.boucekm.miner.it.support.JsonVerifier.jsonPath;
import static cz.boucekm.miner.it.support.RequestBuilder.*;
import static cz.boucekm.miner.it.support.ResultHandler.printResponse;
import static cz.boucekm.miner.it.support.StatusVerifier.status;

/**
 * Integration test for "/miner" service.
 *
 * @author Marian Bouček
 */
@RunWith(LightAir.class)
@Setup
@Verify
public class MinerServiceIT {

    private RestService service;

    @Before
    public void setUp() {
        service = new RestService();
    }

    @Test
    @Verify("MinerServiceIT.xml")
    public void testSearch() throws Exception {
        service.perform(
                get("/miner")
        )
        .andDo(printResponse())
        .andExpect(status().isOk())
        .andExpect(jsonPath("$success").value(Boolean.TRUE))
        .andExpect(jsonPath("$total").value(3))
        .andExpect(jsonPath("$records[0].id").value(1))
        .andExpect(jsonPath("$records[0].name").value("Alan Kay"))
        .andExpect(jsonPath("$records[0].birth").value("1940-05-17T00:00:00.000+0100"))
        .andExpect(jsonPath("$records[0].buried").value(false))
        .andExpect(jsonPath("$records[0].version").value(1))
        .andExpect(jsonPath("$records[1].id").value(2))
        .andExpect(jsonPath("$records[1].name").value("John Carmack"))
        .andExpect(jsonPath("$records[1].birth").value("1970-08-20T00:00:00.000+0100"))
        .andExpect(jsonPath("$records[1].buried").value(false))
        .andExpect(jsonPath("$records[1].version").value(10))
        .andExpect(jsonPath("$records[2].id").value(3))
        .andExpect(jsonPath("$records[2].name").value("Steve Jobs"))
        .andExpect(jsonPath("$records[2].birth").value("1955-02-24T00:00:00.000+0000"))
        .andExpect(jsonPath("$records[2].buried").value(true))
        .andExpect(jsonPath("$records[2].version").value(20));
    }

    @Test
    @Verify("MinerServiceIT.xml")
    public void testFindById() throws Exception {
        service.perform(
                get("/miner/1")
        )
        .andDo(printResponse())
        .andExpect(status().isOk())
        .andExpect(jsonPath("$success").value(Boolean.TRUE))
        .andExpect(jsonPath("$total").value(1))
        .andExpect(jsonPath("$records[0].id").value(1))
        .andExpect(jsonPath("$records[0].name").value("Alan Kay"))
        .andExpect(jsonPath("$records[0].birth").value("1940-05-17T00:00:00.000+0100"))
        .andExpect(jsonPath("$records[0].buried").value(false))
        .andExpect(jsonPath("$records[0].version").value(1));
    }

    @Test
    public void testInsert() throws Exception {
        service.perform(
                post("/miner/0")
                        .content("{\"name\":\"Arnold Schwarzenegger\"," +
                                "\"birth\":\"1947-07-30T00:00:00.000+0100\"," +
                                "\"buried\":false," +
                                "\"version\":0}")
        )
        .andExpect(status().isCreated())
        .andExpect(jsonPath("$success").value(Boolean.TRUE))
        .andExpect(jsonPath("$records[0].id").exists());
    }

    @Test
    public void testUpdate() throws Exception {
        service.perform(
                put("/miner/2")
                        .content("{\"id\":2," +
                                "\"name\":\"Mr. Crow\"," +
                                "\"birth\":\"1955-02-24T00:00:00.000+0000\"," +
                                "\"buried\":true," +
                                "\"version\":10}")
        )
        .andExpect(status().isOk())
        .andExpect(jsonPath("$success").value(Boolean.TRUE));
    }

    @Test
    @Verify("MinerServiceIT.xml")
    public void testUpdateWithBadVersion() throws Exception {
        service.perform(
                put("/miner/2")
                        .content("{\"id\":2," +
                                "\"name\":\"Mr. Crow\"," +
                                "\"birth\":\"1955-02-24T00:00:00.000+0100\"," +
                                "\"buried\":true," +
                                "\"version\":11}")
        )
        .andExpect(status().isOk())
        .andExpect(jsonPath("$success").value(Boolean.FALSE))
        .andExpect(jsonPath("$message").value("Unable to save record because it was changed in a meantime by another user."));
    }

    @Test
    public void testDelete() throws Exception {
        service.perform(
                delete("/miner/2")
                        .content("{\"id\":2," +
                                "\"name\":\"John Carmack\"," +
                                "\"birth\":\"1970-08-20T00:00:00.000+0100\"," +
                                "\"buried\":false," +
                                "\"version\":10}")
        )
        .andExpect(status().isOk())
        .andExpect(jsonPath("$success").value(Boolean.TRUE));
    }

    @Test
    @Verify("MinerServiceIT.xml")
    public void testDeleteWithBadVersion() throws Exception {
        service.perform(
                delete("/miner/2")
                        .content("{\"id\":2," +
                                "\"name\":\"John Carmack\"," +
                                "\"birth\":\"1970-08-20T00:00:00.000+0100\"," +
                                "\"buried\":false," +
                                "\"version\":11}")
        )
        .andExpect(status().isOk())
        .andExpect(jsonPath("$success").value(Boolean.FALSE))
        .andExpect(jsonPath("$message").value("Unable to delete record because it was changed in a meantime by another user."));
    }
}
