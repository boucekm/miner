package cz.boucekm.miner.it.support;

/**
 * Verifier of JSON output.
 *
 * @author Marian Bouček
 */
public class JsonVerifier {

    private final String path;

    private Object expectedValue;
    private Boolean expectedExistence;

    private JsonVerifier(String path) {
        this.path = path;
    }

    public static JsonVerifier jsonPath(String path) {
        return new JsonVerifier(path);
    }

    public JsonVerifier value(String value) {
        this.expectedValue = value;
        return this;
    }

    public JsonVerifier value(boolean value) {
        this.expectedValue = value;
        return this;
    }

    public JsonVerifier value(int value) {
        this.expectedValue = value;
        return this;
    }

    public JsonVerifier exists() {
        this.expectedExistence = Boolean.TRUE;
        return this;
    }

    String getPath() {
        return path;
    }

    Object getExpectedValue() {
        return expectedValue;
    }

    Boolean getExpectedExistence() {
        return expectedExistence;
    }
}
