package cz.boucekm.miner.it.support;

import org.apache.http.HttpStatus;

/**
 * Verifier of returned HTTP status.
 *
 * @author Marian Bouček
 */
public class StatusVerifier {

    private int expectedCode;

    public static StatusVerifier status() {
        return new StatusVerifier();
    }

    public StatusVerifier isOk() {
        expectedCode = HttpStatus.SC_OK;
        return this;
    }

    public StatusVerifier isCreated() {
        expectedCode = HttpStatus.SC_CREATED;
        return this;
    }

    int getExpectedCode() {
        return expectedCode;
    }
}
