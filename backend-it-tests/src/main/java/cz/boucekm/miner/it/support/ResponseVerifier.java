package cz.boucekm.miner.it.support;

import com.jayway.jsonpath.JsonPath;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.InvalidPathException;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertThat;

/**
 * HTTP response verifier.
 */
public class ResponseVerifier {

    private final StatusLine statusLine;
    private final String response;

    ResponseVerifier(HttpResponse response) throws IOException {
        this.statusLine = response.getStatusLine();
        this.response = EntityUtils.toString(response.getEntity(), Charset.forName("UTF-8"));
    }

    public ResponseVerifier andExpect(cz.boucekm.miner.it.support.StatusVerifier statusVerifier) {
        assertThat("Unable to obtain status line.", statusLine, notNullValue());

        int actualStatusCode = statusLine.getStatusCode();
        int expectedCode = statusVerifier.getExpectedCode();

        assertThat("Status", actualStatusCode, is(expectedCode));
        return this;
    }

    public ResponseVerifier andExpect(JsonVerifier jsonVerifier) {

        String path = jsonVerifier.getPath();
        assertThat("Path must be specified.", path, notNullValue());

        Object expectedValue = jsonVerifier.getExpectedValue();
        if (expectedValue != null) {
            Object actualValue = evaluateJsonPath(path);
            assertThat("JSON path " + path, actualValue, is(expectedValue));
        }

        Boolean expectedExistence = jsonVerifier.getExpectedExistence();
        if (expectedExistence != null) {

            Object actualValue = evaluateJsonPath(path);
            if (expectedExistence) {
                assertThat("JSON path " + path, actualValue, notNullValue());
            }
            else {
                assertThat("JSON path " + path, actualValue, nullValue());
            }
        }

        return this;
    }

    public ResponseVerifier andDo(ResultHandler handler) {
        handler.setJsonResponse(response);
        handler.performAction();
        return this;
    }

    private Object evaluateJsonPath(String path) {
        try {
            return JsonPath.compile(path).read(response);
        }
        catch (InvalidPathException e) {
            throw new AssertionError(e.getLocalizedMessage());
        }
        catch (ArrayIndexOutOfBoundsException e) {
            return null;
        }
        catch (IndexOutOfBoundsException e) {
            return null;
        }
    }
}
