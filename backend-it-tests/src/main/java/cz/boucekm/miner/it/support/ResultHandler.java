package cz.boucekm.miner.it.support;

/**
 * Received response handler.
 *
 * @author Marian Bouček
 */
public abstract class ResultHandler {

    private String jsonResponse;

    /**
     * Print received JSON to console.
     */
    public static ResultHandler printResponse() {
        return new ConsolePrintingResultHandler();
    }

    // private API ----------------------------------------------------------------
    private ResultHandler() {
        super();
    }

    abstract void performAction();

    String getJsonResponse() {
        return jsonResponse;
    }

    void setJsonResponse(String jsonResponse) {
        this.jsonResponse = jsonResponse;
    }

    private static class ConsolePrintingResultHandler extends ResultHandler {

        @Override
        void performAction() {
            System.out.println("Received JSON: " + getJsonResponse() + "\n");
        }
    }
}
