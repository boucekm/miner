package cz.boucekm.miner.it.support;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpEntityEnclosingRequestBase;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Map;

import static cz.boucekm.miner.it.support.RequestBuilder.RequestMethod;
import static org.apache.commons.lang.StringUtils.isBlank;
import static org.apache.http.impl.client.HttpClientBuilder.create;

/**
 * Rest service endpoint.
 *
 * @author Marian Bouček
 */
public class RestService {

    private final int port;
    private final String host;
    private final String pathPrefix;

    public RestService() {
        this("localhost", "/backend", 8090);
    }

    /**
     * Only for debug testing. Production code should always use default values.
     */
    public RestService(String host, String pathPrefix, int port) {
        this.host = host;
        this.port = port;
        this.pathPrefix = pathPrefix;
    }

    public ResponseVerifier perform(RequestBuilder requestBuilder) {

        RequestMethod method = requestBuilder.getMethod();
        assertParam("No request specified.", method);

        String path = requestBuilder.getPath();
        assertParam("Path is required.", path);

        URI uri = buildURIWithParameters(path, requestBuilder);
        GenericHttpMethod request = new GenericHttpMethod(uri, method);

        fillBody(requestBuilder, request);

        try (CloseableHttpClient client = create().build()) {
            HttpResponse execute = client.execute(request);
            return new ResponseVerifier(execute);
        } catch (IOException e) {
            throw new AssertionError("Unable to retrieve server response.", e);
        }
    }

    private URI buildURIWithParameters(String path, RequestBuilder requestBuilder) {

        URIBuilder uriBuilder = new URIBuilder()
                .setScheme("http")
                .setHost(host)
                .setPort(port)
                .setPath(pathPrefix + path);

        Map<String,String> parameters = requestBuilder.getParameters();
        for (Map.Entry<String, String> entry : parameters.entrySet()) {
            uriBuilder.addParameter(entry.getKey(), entry.getValue());
        }

        try {
            return uriBuilder.build();
        } catch (URISyntaxException e) {
            throw new IllegalArgumentException(e.getMessage());
        }
    }

    private void fillBody(RequestBuilder requestBuilder, GenericHttpMethod request) {
        String requestBody = requestBuilder.getRequestBody();
        if (isBlank(requestBody)) {
            return;
        }

        StringEntity entity = new StringEntity(requestBody, "UTF-8");
        entity.setContentType("application/json");

        request.setEntity(entity);
    }

    private static void assertParam(String errMsg, Object value) {
        if (value == null) {
            throw new AssertionError(errMsg);
        }
    }

    private static class GenericHttpMethod extends HttpEntityEnclosingRequestBase {

        private final String method;

        /**
         * @throws IllegalArgumentException if the uri is invalid.
         */
        public GenericHttpMethod(final URI uri, final RequestMethod method) {
            setURI(uri);
            this.method = method.name();
        }

        @Override
        public String getMethod() {
            return method;
        }
    }
}
