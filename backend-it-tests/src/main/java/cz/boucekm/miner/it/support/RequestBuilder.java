package cz.boucekm.miner.it.support;

import java.util.HashMap;
import java.util.Map;

/**
 * Builder for creating request.
 *
 * @author Marian Bouček
 */
public class RequestBuilder {

    private final Map<String, String> parameters;
    private final RequestMethod method;
    private final String path;

    private String requestBody;

    public RequestBuilder(RequestMethod method, String path) {
        this.parameters = new HashMap<>();
        this.method = method;
        this.path = path;
    }

    public static RequestBuilder get(String path) {
        return new RequestBuilder(RequestMethod.GET, path);
    }

    public static RequestBuilder put(String path) {
        return new RequestBuilder(RequestMethod.PUT, path);
    }

    public static RequestBuilder post(String path) {
        return new RequestBuilder(RequestMethod.POST, path);
    }

    public static RequestBuilder delete(String path) {
        return new RequestBuilder(RequestMethod.DELETE, path);
    }

    public RequestBuilder param(String key, String value) {
        parameters.put(key, value);
        return this;
    }

    public RequestBuilder content(String content) {
        this.requestBody = content;
        return this;
    }

    Map<String, String> getParameters() {
        return parameters;
    }

    RequestMethod getMethod() {
        return method;
    }

    String getPath() {
        return path;
    }

    String getRequestBody() {
        return requestBody;
    }

    static enum RequestMethod {
        GET,
        PUT,
        POST,
        DELETE
    }
}
