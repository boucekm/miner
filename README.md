# README

This is demo for Spring Boot presentation. It contains all necessary files to build and run application from command line or from your favorite IDE.

## Prerequisites

* PostgreSQL (OS X users can use http://postgresapp.com)
* Java 8
* Maven 3

## Run

* Run cz.boucekm.miner.Application from IDE (will use in-memory db)
* Open frontend/build/production/Miner/index.html in your browser
* For Postgres set --spring.profiles.active=postgre as Program arguments

## Run integration tests (Postgres)

* Launch postgres
* Run following SQL commands:
    CREATE DATABASE miner;
    CREATE DATABASE miner_test;
* (Optional) Copy connection properties from ./pom.xml to your local ~/.m2/settings.xml
* Build application with: mvn install
