package cz.boucekm.miner.controller;

import cz.boucekm.miner.controller.response.GenericResponse;
import cz.boucekm.miner.input.MinerSearch;
import cz.boucekm.miner.to.Miner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.sqlproc.engine.SqlCrudEngine;
import org.sqlproc.engine.SqlQueryEngine;
import org.sqlproc.engine.SqlSession;
import org.sqlproc.engine.jdbc.JdbcEngineFactory;
import org.sqlproc.engine.spring.SpringSessionFactory;

import javax.validation.Valid;
import java.util.List;

import static cz.boucekm.miner.controller.response.ResponseBuilder.*;
import static org.springframework.web.bind.annotation.RequestMethod.*;

/**
 * REST controller for miner's data.
 *
 * @author Marian Bouček
 */
@Controller
@ResponseBody
@RequestMapping(value = "/miner")
public class MinerController {

    private static final Logger LOGGER = LoggerFactory.getLogger(MinerController.class);

    @Autowired
    private JdbcEngineFactory engineFactory;

    @Autowired
    private SpringSessionFactory sessionFactory;

    @RequestMapping(method = GET)
    public GenericResponse search(MinerSearch search) {
        LOGGER.info("GET /params={}", search);

        SqlQueryEngine engine = engineFactory.getQueryEngine("ALL_MINERS");

        List<Miner> miners = null;
        int count = engine.queryCount(getSqlSession(), search);

        if (count > 0) {
            miners = engine.query(getSqlSession(), Miner.class, search, search.getLimit(), search.getStart());
        }

        return getResponse(count, miners);
    }

    @RequestMapping(value = "/{id}", method = GET)
    public GenericResponse findById(@PathVariable int id) {
        LOGGER.info("GET /id={}", id);

        SqlQueryEngine engine = engineFactory.getQueryEngine("ALL_MINERS");

        MinerSearch search = new MinerSearch();
        search.setId(id);

        List<Miner> miners = null;
        int count = engine.queryCount(getSqlSession(), search);

        if (count > 0) {
            miners = engine.query(getSqlSession(), Miner.class, search, search.getLimit(), search.getStart());
        }

        return getResponse(count, miners);
    }

    @RequestMapping(value = "/{id}", method = POST)
    @ResponseStatus(HttpStatus.CREATED)
    public GenericResponse insert(@PathVariable int id, @Valid @RequestBody Miner miner) {
        LOGGER.info("POST /data={}", miner);

        SqlCrudEngine engine = engineFactory.getCheckedCrudEngine("INSERT_MINER");
        int result = engine.insert(getSqlSession(), miner);

        return insertResponse(result, miner.getId());
    }

    @RequestMapping(value = "/{id}", method = PUT)
    public GenericResponse update(@PathVariable int id, @Valid @RequestBody Miner miner) {
        LOGGER.info("PUT /data={}", miner);

        SqlCrudEngine engine = engineFactory.getCrudEngine("UPDATE_MINER");
        int update = engine.update(getSqlSession(), miner);

        return updateResponse(update);
    }

    @RequestMapping(value = "/{id}", method = DELETE)
    public GenericResponse deleteById(@PathVariable int id, @Valid @RequestBody Miner miner) {
        LOGGER.info("DELETE /id={}", id);

        SqlCrudEngine engine = engineFactory.getCrudEngine("DELETE_MINER");
        int result = engine.delete(getSqlSession(), new Miner(id, miner.getVersion()));

        return deleteResponse(result);
    }

    private SqlSession getSqlSession() {
        return sessionFactory.getSqlSession();
    }
}
