package cz.boucekm.miner.controller.response;

import org.apache.commons.lang.builder.ToStringBuilder;

import java.util.List;

/**
 * Generic response which is required by Ext.js framework.
 *
 * @author Marian Bouček
 */
@SuppressWarnings("UnusedDeclaration")
public class GenericResponse {

	private boolean success;
	private List<?> records;
	private Integer total;
	private String message;

	GenericResponse() {
		super();
	}

	public boolean isSuccess() {
		return success;
	}

	public GenericResponse setSuccess(boolean success) {
		this.success = success;
		return this;
	}

	public List<?> getRecords() {
		return records;
	}

	public GenericResponse setRecords(List<?> records) {
		this.records = records;
		return this;
	}

	public Integer getTotal() {
		return total;
	}

	public GenericResponse setTotal(Integer total) {
		this.total = total;
		return this;
	}

	public String getMessage() {
		return message;
	}

	public GenericResponse setMessage(String message) {
		this.message = message;
		return this;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this)
				.append("success", success)
				.append("records", records)
				.append("total", total)
				.append("message", message)
				.toString();
	}
}
