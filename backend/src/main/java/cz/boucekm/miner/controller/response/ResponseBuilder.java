package cz.boucekm.miner.controller.response;

import java.util.List;

import static java.util.Collections.singletonList;

/**
 * Builder for creating unified JSON responses.
 *
 * @author Marian Bouček
 */
public class ResponseBuilder {

	/**
	 * Creates error response.
	 *
	 * @param message error message
	 */
	public static GenericResponse error(String message) {
		return new GenericResponse().setSuccess(false).setMessage(message);
	}

	/**
	 * Creates response for <code>GET</code> operation.
	 *
	 * @param total total records count
	 * @param records found records (can be <code>null</code>)
	 * @return It always returns successful response.
	 */
	public static GenericResponse getResponse(int total, List<?> records) {
		return new GenericResponse().setSuccess(true).setTotal(total).setRecords(records);
	}

	/**
	 * Creates response for <code>INSERT</code> operation.
	 *
	 * @param count count of changed rows
	 * @param id ID of newly created record (it must be zero in case of error)
	 * @return Return error response in case count is not equal to one.
	 */
	public static GenericResponse insertResponse(int count, long id) {
		if (count != 1) {
			return new GenericResponse()
					.setSuccess(false)
					.setMessage("Unable to create new record.");
		}

		return new GenericResponse()
				.setSuccess(true)
				.setRecords(singletonList(new IdentityResponse(id)));
	}

	/**
	 * Creates response for <code>UPDATE</code> operation.
	 *
	 * @param count updated rows count (from database operation)
	 * @return Returns success if count is equal to one. Multiple update is not supported.
	 */
	public static GenericResponse updateResponse(int count) {
		boolean updateSuccessful = count == 1;
		GenericResponse response = new GenericResponse().setSuccess(updateSuccessful);

		if (!updateSuccessful) {
			response.setMessage("Unable to save record because it was changed in a meantime by another user.");
		}

		return response;
	}

	/**
	 * Creates response for <code>DELETE</code> operation.
	 *
	 * @param count count of changed rows
	 * @return Returns success if count is equal to one. Multiple update is not supported.
	 */
	public static GenericResponse deleteResponse(int count) {
		boolean deleteSuccessful = count == 1;
		GenericResponse response = new GenericResponse().setSuccess(deleteSuccessful);

		if (!deleteSuccessful) {
			response.setMessage("Unable to delete record because it was changed in a meantime by another user.");
		}

		return response;
	}
}
