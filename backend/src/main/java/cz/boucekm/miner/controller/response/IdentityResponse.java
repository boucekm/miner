package cz.boucekm.miner.controller.response;

import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * Response with identity only.
 *
 * @author Marian Bouček
 */
@SuppressWarnings("UnusedDeclaration")
public class IdentityResponse {

	private final long id;

	public IdentityResponse(long id) {
		this.id = id;
	}

	public long getId() {
		return id;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this)
				.append("id", id)
				.toString();
	}
}
