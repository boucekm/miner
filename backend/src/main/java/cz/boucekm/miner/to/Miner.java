package cz.boucekm.miner.to;

import cz.boucekm.miner.validator.DateRange;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * Transfer object for Miner.
 */
@SuppressWarnings("UnusedDeclaration")
public class Miner {

    private Integer id;

    @NotBlank
    private String name;

    @NotNull
    @DateRange(start = "01.01.1900")
    private Date birth;

    @NotNull
    private Boolean buried;

    @NotNull
    private Integer version;

    public Miner() {
        super();
    }

    public Miner(int id, Integer version) {
        this.id = id;
        this.version = version;
    }

    public Miner(Integer id, String name, Date birth, Boolean buried, Integer version) {
        this.id = id;
        this.name = name;
        this.birth = birth;
        this.buried = buried;
        this.version = version;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getBirth() {
        return birth;
    }

    public void setBirth(Date birth) {
        this.birth = birth;
    }

    public Boolean getBuried() {
        return buried;
    }

    public void setBuried(Boolean buried) {
        this.buried = buried;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("id", id)
                .append("name", name)
                .append("birth", birth)
                .append("buried", buried)
                .append("version", version)
                .toString();
    }
}
