package cz.boucekm.miner.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.apache.commons.lang.StringUtils.isBlank;

/**
 * Validator implementation for {@link cz.boucekm.miner.validator.DateRange}.
 *
 * @author Marian Bouček
 */
public class DateRangeValidator implements ConstraintValidator<cz.boucekm.miner.validator.DateRange, Date> {

    private Date start;
    private Date end;

    @Override
    public void initialize(cz.boucekm.miner.validator.DateRange constraintAnnotation) {

        if (isBlank(constraintAnnotation.start()) && isBlank(constraintAnnotation.end())) {
            throw new IllegalArgumentException("Annotation doesn't have neither start or end specified.");
        }

        start = parseDate(constraintAnnotation.start());
        end = parseDate(constraintAnnotation.end());
    }

    @Override
    public boolean isValid(Date value, ConstraintValidatorContext context) {

        if (value == null) {
            return true;
        }

        // only end
        if (start == null) {
            return !end.before(value);
        }

        // only start
        if (end == null) {
            return !value.before(start);
        }

        // otherwise validate for both
        return value.after(start) && value.before(end);
    }

    private static Date parseDate(String date) {
        if (isBlank(date)) {
            return null;
        }

        try {
            return new SimpleDateFormat("dd.MM.yyyy").parse(date);
        } catch (ParseException e) {
            throw new IllegalArgumentException("Invalid date format!");
        }
    }
}
