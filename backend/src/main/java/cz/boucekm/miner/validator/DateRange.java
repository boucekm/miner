package cz.boucekm.miner.validator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Validation for date range. At least <code>start</code> or <code>end</code> must be specified.
 *
 * @author Marian Bouček
 */
@SuppressWarnings("UnusedDeclaration")
@Documented
@Constraint(validatedBy = { DateRangeValidator.class })
@Target({ METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER })
@Retention(RUNTIME)
public @interface DateRange {

    String message() default "{cz.boucekm.miner.validator.NotBlank.message}";

    Class<?>[] groups() default { };

    Class<? extends Payload>[] payload() default { };

    String start() default "";

    String end() default "";

}
