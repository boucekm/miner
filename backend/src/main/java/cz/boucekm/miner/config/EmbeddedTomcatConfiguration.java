package cz.boucekm.miner.config;

import org.springframework.boot.context.embedded.EmbeddedServletContainerFactory;
import org.springframework.boot.context.embedded.tomcat.TomcatEmbeddedServletContainerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

/**
 * Configuration of embedded Tomcat server. It will force <code>UTF-8</code> encoding in URI encoding.
 *
 * @author Marian Boucek
 */
@Configuration
@Profile({"default", "mssql"})
public class EmbeddedTomcatConfiguration {

    @Bean
    public EmbeddedServletContainerFactory servletContainer() {
        TomcatEmbeddedServletContainerFactory factory = new TomcatEmbeddedServletContainerFactory(8080);
        factory.addConnectorCustomizers(connector -> connector.setURIEncoding("UTF-8"));

        return factory;
    }
}
