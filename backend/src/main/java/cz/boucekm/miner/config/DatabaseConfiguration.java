package cz.boucekm.miner.config;

import com.googlecode.flyway.core.Flyway;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jndi.JndiTemplate;
import org.sqlproc.engine.jdbc.JdbcEngineFactory;
import org.sqlproc.engine.spring.SpringSessionFactory;

import javax.naming.NamingException;
import javax.sql.DataSource;

/**
 * Database configuration for SQL processor.
 *
 * @author Marian Boucek
 */
@Configuration
public class DatabaseConfiguration {

    private static final Logger LOGGER = LoggerFactory.getLogger(DatabaseConfiguration.class);

    @Autowired
    private JdbcTemplate jdbcTemplate;

    // taken from application*.properties file
    @Value("${sqlprocessor.engine.filter}")
    private String engingeFilter;

    @Bean
    public SpringSessionFactory createSessionFactory() {
        return new SpringSessionFactory(jdbcTemplate);
    }

    /**
     * Creates JDBC engine factory for SQL processor.
     */
    @Bean
    public JdbcEngineFactory createEngineFactory() {
        LOGGER.info("Creating JDBC engine factory (filter={}).", engingeFilter);

        JdbcEngineFactory jdbcEngineFactory = new JdbcEngineFactory();

        jdbcEngineFactory.setMetaFilesNames("statements.qry");
        jdbcEngineFactory.setFilter(engingeFilter);

        return jdbcEngineFactory;
    }

    /**
     * Execute development db migration on application startup.
     */
    @Bean
    @Profile({"default", "postgre"})
    public Flyway createFlywayMigration() {
        return executeMigrationForLocations("classpath:/db/production", "classpath:/db/development");
    }

    /**
     * Execute production db migration on application startup.
     */
    @Bean
    @Profile("production")
    public Flyway createProductionFlywayMigration() {
        return executeMigrationForLocations("classpath:/db/production");
    }

    private Flyway executeMigrationForLocations(String... locations) {
        LOGGER.info("Executing Flyway migration for {} database...", engingeFilter);
        Flyway flyway = new Flyway();

        flyway.setLocations(locations);
        flyway.setSqlMigrationPrefix("v");
        flyway.setDataSource(jdbcTemplate.getDataSource());

        int migrate = flyway.migrate();
        LOGGER.info("Done (number of migrations = {}).", migrate);

        return flyway;
    }

    /**
     * Creates JDBC template for standalone application server (profile <code>production</code>).
     */
    @Bean
    @Profile("production")
    public JdbcTemplate createTemplateForStandaloneAppServer() throws NamingException {
        LOGGER.info("Creating JDBC template from JNDI datasource.");

        DataSource dataSource = (DataSource) new JndiTemplate().lookup("java:comp/env/jdbc/miner");
        return new JdbcTemplate(dataSource);
    }
}
