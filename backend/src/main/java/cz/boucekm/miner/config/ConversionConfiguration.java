package cz.boucekm.miner.config;

import com.fasterxml.jackson.annotation.JsonInclude;
import org.springframework.context.annotation.Configuration;
import org.springframework.format.FormatterRegistry;
import org.springframework.format.datetime.DateFormatter;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

import java.text.SimpleDateFormat;
import java.util.List;

/**
 * Conversion configuration for date types and JSON.
 */
@Configuration
public class ConversionConfiguration extends WebMvcConfigurationSupport {

    private static final String DATE_PATTERN = "yyyy-MM-dd'T'HH:mm:ss.SSSZ";

    /**
     * Custom data format with timezone support.
     */
    @Override
    protected void addFormatters(FormatterRegistry registry) {
        registry.addFormatter(new DateFormatter(DATE_PATTERN));
    }

    /**
     * Definition of JSON converter via Jackson library.
     */
    @Override
    protected void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
        converters.add(jacksonConverter());
    }

    /**
     * Creates JSON converter. All <code>null</code> values are ommited in result.
     */
    public MappingJackson2HttpMessageConverter jacksonConverter() {
        MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();

        converter.getObjectMapper()
                .setDateFormat(new SimpleDateFormat(DATE_PATTERN))
                .setSerializationInclusion(JsonInclude.Include.NON_NULL);

        return converter;
    }
}
