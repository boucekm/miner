package cz.boucekm.miner.config.handler;

import cz.boucekm.miner.controller.response.GenericResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.http.HttpStatus;
import org.springframework.jdbc.BadSqlGrammarException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.sqlproc.engine.SqlProcessorException;

import javax.servlet.http.HttpServletRequest;

import static cz.boucekm.miner.controller.response.ResponseBuilder.error;

/**
 * Database exception handler so exceptions will not be propagated to frontend.
 *
 * @author Marian Bouček
 */
@ControllerAdvice
public class DatabaseExceptionHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(DatabaseExceptionHandler.class);

    @ResponseStatus(HttpStatus.BAD_REQUEST) // 400
    @ExceptionHandler(value = SqlProcessorException.class)
    @ResponseBody
    public GenericResponse handleSqlProcessorException(HttpServletRequest req, SqlProcessorException e) {
        LOGGER.debug("Caught SQL exception. Reason = {}", e.getCause());

        Throwable cause = e.getCause();
        if (cause instanceof DuplicateKeyException) {
            LOGGER.debug("Unable to create duplicate record in database: {}", e.getMessage());
            return error("Unable to create duplicate record in database.");
        }

        if (cause instanceof BadSqlGrammarException) {
            LOGGER.error("Wrong SQL: {}", e.getMessage());
            return error("Unexpected application error.");
        }

        LOGGER.error("Unexpected SQL exception: {}", e.getMessage());
        return error("Unexpected database error.");
    }
}
