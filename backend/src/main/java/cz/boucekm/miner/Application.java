package cz.boucekm.miner;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.web.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * Main application class.
 *
 * @author Marian Boucek
 */
@Configuration
@ComponentScan
@EnableAutoConfiguration
public class Application extends SpringBootServletInitializer {

    /**
     * Configuration for execution in Tomcat application server. It will activate
     * <code>production</code> Spring profile.
     */
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(Application.class).profiles("production");
    }

    /**
     * Main method for running application from command line (standalone mode).
     *
     * <ul>
     *     <li><b>without any argument</b> - will use embedded HSQLDB</li>
     *     <li><b>--spring.profiles.active=postgre</b> - external database Postgres</li>
     *     <li><b>--debug</b> - debug mode for Spring Boot</li>
     * </ul>
     *
     * @param args argument will be passed to Spring application context
     */
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
