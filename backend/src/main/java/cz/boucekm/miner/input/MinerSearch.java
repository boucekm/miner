package cz.boucekm.miner.input;

import org.apache.commons.lang.builder.ToStringBuilder;

import java.util.Date;

/**
 * Search criteria for Miner.
 *
 * @author Marian Boucek
 */
@SuppressWarnings("UnusedDeclaration")
public class MinerSearch {

    private Integer id;

    private int limit;
    private int start;

    private String name;
    private Date birth;
    private Boolean buried;

    public MinerSearch() {
        super();
    }

    public MinerSearch(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public int getStart() {
        return start;
    }

    public void setStart(int start) {
        this.start = start;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getBirth() {
        return birth;
    }

    public void setBirth(Date birth) {
        this.birth = birth;
    }

    public Boolean getBuried() {
        return buried;
    }

    public void setBuried(Boolean buried) {
        this.buried = buried;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("id", id)
                .append("limit", limit)
                .append("start", start)
                .append("name", name)
                .append("birth", birth)
                .append("buried", buried)
                .toString();
    }
}
