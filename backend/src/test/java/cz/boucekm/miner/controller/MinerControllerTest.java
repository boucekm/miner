package cz.boucekm.miner.controller;

import cz.boucekm.miner.input.MinerSearch;
import cz.boucekm.miner.to.Miner;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static cz.boucekm.miner.controller.response.ResponseBuilder.*;
import static cz.boucekm.miner.controller.support.StandaloneSetupBuilder.buildDefaultSetup;
import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.refEq;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Unit test for MinerController.
 *
 * @author Marian Bouček
 */
public class MinerControllerTest {

    private static final SimpleDateFormat SIMPLE_DATE_FORMAT = new SimpleDateFormat("dd.MM.yyyy");

    private MockMvc mockMvc;

    @Mock
    private MinerController controller;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        mockMvc = buildDefaultSetup(controller);
    }

    @Test
    public void testList() throws Exception {
        List<Miner> expectedResult = asList(
                JohnCarmack(),
                SteveJobs());

        when(controller.search(refEq(new MinerSearch()))).thenReturn(getResponse(expectedResult.size(), expectedResult));

        mockMvc.perform(
                get("/miner")
                        .accept(MediaType.APPLICATION_JSON)
        )
        .andExpect(status().isOk())
        .andExpect(jsonPath("$success").value(Boolean.TRUE))
        .andExpect(jsonPath("$records[0].id").value(1))
        .andExpect(jsonPath("$records[0].name").value("John Carmack"))
        .andExpect(jsonPath("$records[0].birth").value("1970-08-20T00:00:00.000+0100"))
        .andExpect(jsonPath("$records[0].buried").value(false))
        .andExpect(jsonPath("$records[0].version").value(10))
        .andExpect(jsonPath("$records[1].id").value(2))
        .andExpect(jsonPath("$records[1].name").value("Steve Jobs"))
        .andExpect(jsonPath("$records[1].birth").value("1955-02-24T00:00:00.000+0000"))
        .andExpect(jsonPath("$records[1].buried").value(true))
        .andExpect(jsonPath("$records[1].version").value(20));
    }

    @Test
    public void testFindById() throws Exception {
        when(controller.findById(eq(1))).thenReturn(getResponse(1, singletonList(JohnCarmack())));

        mockMvc.perform(
                get("/miner/1")
                        .accept(MediaType.APPLICATION_JSON)
        )
        .andExpect(status().isOk())
        .andExpect(jsonPath("$success").value(Boolean.TRUE))
        .andExpect(jsonPath("$records[0].id").value(1))
        .andExpect(jsonPath("$records[0].name").value("John Carmack"))
        .andExpect(jsonPath("$records[0].birth").value("1970-08-20T00:00:00.000+0100"))
        .andExpect(jsonPath("$records[0].buried").value(false))
        .andExpect(jsonPath("$records[0].version").value(10));
    }

    @Test
    public void testInsert() throws Exception {
        Miner alanKay = AlanKay();
        when(controller.insert(eq(alanKay.getId()), refEq(alanKay))).thenReturn(insertResponse(1, 101));

        mockMvc.perform(
                post("/miner/0")
                        .content("{\"id\": 0, \"name\":\"Alan Kay\",\"birth\":\"1940-05-17T00:00:00.000+0100\",\"buried\":false,\"version\":1}")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
        )
        .andExpect(status().isCreated())
        .andExpect(jsonPath("$success").value(Boolean.TRUE))
        .andExpect(jsonPath("$records[0].id").value(101));
    }

    @Test
    public void testUpdate() throws Exception {
        Miner steveJobs = SteveJobs();
        when(controller.update(eq(steveJobs.getId()), refEq(steveJobs))).thenReturn(updateResponse(1));

        // In case that our method isn't called as we expected, use this for debugging purposes
        // when(controller.update(eq(2), isNotNull(Miner.class))).thenCallRealMethod();

        mockMvc.perform(
                put("/miner/2")
                        .content("{\"id\":2,\"name\":\"Steve Jobs\",\"birth\":\"1955-02-24T00:00:00.000+0000\",\"buried\":true,\"version\":20}")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
        )
        .andExpect(status().isOk())
        .andExpect(jsonPath("$success").value(Boolean.TRUE));
    }

    @Test
    public void testUpdateEmptyName() throws Exception {
        mockMvc.perform(
                put("/miner/1")
                        .content("{\"id\":1,\"name\":\"   \"}")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
        )
        .andExpect(status().isBadRequest());
    }

    @Test
    public void testUpdateEmptyDate() throws Exception {
        mockMvc.perform(
                put("/miner/1")
                        .content("{\"id\":1,\"name\":\"John Carmack\",\"birth\":null}")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
        )
        .andExpect(status().isBadRequest());
    }

    @Test
    public void testUpdateZombie() throws Exception {
        mockMvc.perform(
                put("/miner/1")
                        .content("{\"id\":1,\"name\":\"Zombie\",\"birth\":\"1800-01-01T00:00:00.000+0100\"}")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
        )
        .andExpect(status().isBadRequest());
    }

    @Test
    public void testDeleteById() throws Exception {
        Miner johnCarmack = JohnCarmack();
        when(controller.deleteById(eq(johnCarmack.getId()), refEq(johnCarmack))).thenReturn(deleteResponse(1));

        mockMvc.perform(
                delete("/miner/1")
                        .content("{\"id\":1,\"name\":\"John Carmack\",\"birth\":\"1970-08-20T00:00:00.000+0100\",\"buried\":false,\"version\":10}")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
        )
        .andExpect(status().isOk())
        .andExpect(jsonPath("$success").value(Boolean.TRUE));
    }

    /**
     * Bez udání ID nesmí projít jiná metoda než GET.
     */
    @Test
    public void testForbiddenMethods() throws Exception {
        mockMvc.perform(put("/miner")).andExpect(status().isMethodNotAllowed());
        mockMvc.perform(post("/miner")).andExpect(status().isMethodNotAllowed());
        mockMvc.perform(delete("/miner")).andExpect(status().isMethodNotAllowed());
    }

    private static Miner AlanKay() {
        return new Miner(0, "Alan Kay", date("17.05.1940"), false, 1);
    }

    private static Miner JohnCarmack() {
        return new Miner(1, "John Carmack", date("20.08.1970"), false, 10);
    }

    private static Miner SteveJobs() {
        return new Miner(2, "Steve Jobs", date("24.02.1955"), true, 20);
    }

    private static Date date(String format) {
        try {
            return SIMPLE_DATE_FORMAT.parse(format);
        } catch (ParseException e) {
            throw new IllegalStateException(e);
        }
    }
}
