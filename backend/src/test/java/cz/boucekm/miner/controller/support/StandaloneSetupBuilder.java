package cz.boucekm.miner.controller.support;

import cz.boucekm.miner.config.ConversionConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.AbstractMockMvcBuilder;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

/**
 * Standalone setup builder for MVC tests.
 *
 * @author Marian Bouček
 */
public class StandaloneSetupBuilder {

    /**
     * Creates basic configuration of mock MVC builder.
     *
     * @param controllers list of mocked controllers
     */
    public static AbstractMockMvcBuilder defaultSetup(Object... controllers) {
        ConversionConfiguration application = new ConversionConfiguration();

        return standaloneSetup(controllers)
                .setConversionService(application.mvcConversionService())
                .setMessageConverters(application.jacksonConverter());
    }

    /**
     * Creates configured mock which will log every request to console output.
     *
     * @param controllers list of mocked controllers
     */
    public static MockMvc buildDefaultSetup(Object... controllers) {
        if (controllers.length == 0) {
            throw new IllegalArgumentException("At least one controller must be specified.");
        }

        for (Object o : controllers) {
            if (o == null) {
                throw new IllegalArgumentException(
                        "One of provided controllers is 'null'. " +
                        "Did you initialize mocks? (e.g. with MockitoAnnotations.initMocks(this))");
            }
        }

        return defaultSetup(controllers)
                .alwaysDo(print())
                .build();
    }
}
