package cz.boucekm.miner.validator;

import org.junit.Test;

import javax.validation.Payload;
import java.lang.annotation.Annotation;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * Test for class {@link cz.boucekm.miner.validator.DateRangeValidator}.
 *
 * @author Marian Bouček
 */
public class DateRangeValidatorTest {

    @Test(expected = IllegalArgumentException.class)
    public void testInitializeInvalid() throws Exception {
        createValidator(null, null);
        fail("Validator didn't throw an exception.");
    }

    @Test
    public void testIsValidateNullDate() throws Exception {
        assertTrue(
                createValidator("01.01.2010", null)
                        .isValid(null, null)
        );
    }

    @Test
    public void testIsValidateStart() throws Exception {
        assertTrue(
                createValidator("01.01.2010", null)
                        .isValid(date("01.01.2020"), null)
        );
    }

    @Test
    public void testIsValidateEnd() throws Exception {
        assertTrue(
                createValidator(null, "01.01.2020")
                        .isValid(date("01.01.2010"), null)
        );
    }

    @Test
    public void testIsValidBetween() throws Exception {
        assertTrue(
                createValidator("01.01.2000", "01.01.2020")
                        .isValid(date("01.01.2010"), null)
        );
    }

    private DateRangeValidator createValidator(String start, String end) {
        DateRangeValidator validator = new DateRangeValidator();
        validator.initialize(new DateRangeFakeImpl(start, end));

        return validator;
    }

    private static Date date(String format) {
        try {
            return new SimpleDateFormat("dd.MM.yyyy").parse(format);
        } catch (ParseException e) {
            throw new IllegalStateException(e);
        }
    }

    @SuppressWarnings("ClassExplicitlyAnnotation")
    private static class DateRangeFakeImpl implements DateRange {

        private final String start;
        private final String end;

        public DateRangeFakeImpl(String start, String end) {
            this.start = start;
            this.end = end;
        }

        @Override
        public String message() {
            return null;
        }

        @Override
        public Class<?>[] groups() {
            return null;
        }

        @Override
        public Class<? extends Payload>[] payload() {
            return null;
        }

        @Override
        public String start() {
            return start;
        }

        @Override
        public String end() {
            return end;
        }

        @Override
        public Class<? extends Annotation> annotationType() {
            return null;
        }
    }
}
