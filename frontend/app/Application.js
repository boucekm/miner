Ext.define('Miner.Application', {
    extend: 'Ext.app.Application',
    name: 'Miner',
    appFolder: 'app',
    autoCreateViewport: true,
    
    requires: [ 
        'Miner.view.Viewport',
        
        'Miner.util.Config',
        'Miner.util.Utils',
        'Miner.util.Text',
        
        'Miner.view.main.Main',
        'Miner.view.main.MainWrapper',
        'Miner.view.main.StartupWrapper'
    ],
    
    controllers: [
        'Main'
    ],

    views: [
        'Miner.view.main.Main',
        'Miner.view.main.MainWrapper'
    ],
    
    // before launch()
    init: function() {
        var me = this;
        console.log('Application init...');       
        
        var loading = Ext.fly('loading');
        if (loading) {
            loading.remove();
        }
    },

    // after init()
    launch: function(profile) {
        console.log('Application launched...');
    }
});
