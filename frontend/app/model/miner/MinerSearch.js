Ext.define('Miner.model.miner.MinerSearch', {
    extend: 'Ext.data.Model',

    fields: [
        {
            name: 'name',
            type: 'string'
        },
        {
            name: 'birth',
            type: 'date'
        },
        {
            name: 'buried',
            type: 'boolean'
        }
    ]
});
