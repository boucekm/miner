Ext.define('Miner.model.miner.Miner', {
    extend: 'Ext.data.Model',

    idProperty: 'id',
    fields: [
        {
            name: 'id',
            type: 'int'
        },
        { // property for optimistic db locking
            name: 'version',
            type: 'int'
        },
        {
            name: 'name',
            type: 'string'
        },
        {
            name: 'birth',
            type: 'date'
        },
        {
            name: 'buried',
            type: 'boolean'
        }
    ],
      
    
    proxy: {
        type: 'rest',
        url: Miner.Config.getProxyUrlPrefix() + '/miner',
        appendId: true,       
        noCache: true,        
        cacheString: '_dc',
        pageParam: 'page',
        startParam: 'start',
        limitParam: 'limit',
        timeout: Miner.Config.getProxyTimeout(),
        idParam: 'id',
        
        simpleSortMode: true,     
        sortParam: 'sort',
        directionParam: 'dir',    

        reader: {
            type: 'json',
            root: 'records',
            idProperty: 'id',
            totalProperty: 'total',
            successProperty: 'success',
            messageProperty: 'message'
        },
        
        writer: {
            type: 'json',
            writeAllFields: true,
            writeRecordId: true,
            expandData: true,         
            nameProperty: 'mapping'   
        }
    }
});
