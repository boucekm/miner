Ext.define('Miner.controller.miner.MinerEdit', {
    extend: 'Ext.app.Controller',

    requires: [
        'Miner.model.miner.Miner'    
    ],
    
    views: [
        'miner.MinerEdit',
        'miner.MinerDetail'
    ],    
    
    refs: [
        {
            ref: 'minerMinerEditRef',
            selector: 'miner-MinerEdit'
        },
        {
            ref: 'minerMinerDetailRef',
            selector: 'miner-MinerDetail'
        }
    ],
    
    init: function() {
        this.control({
            'miner-MinerEdit button[action="save"]': {  
                click: this.saveMiner
            },
            'miner-MinerEdit button[action="cancel"]': {  
                click: this.cancelMiner
            }
        });
    },
   
    cancelMiner: function(button) {
        Miner.Utils.mainCardRemoveShowLast();
    },
    
    saveMiner: function(saveButton) {
        var me = this;
      
        var successFn = undefined;
        var detailView = me.getMinerMinerDetailRef() || Ext.widget('miner-MinerDetail'); 
        var detailRecordClass = Miner.model.miner.Miner;
        var silent = true;
        var beforeSaveFn = undefined;
        Miner.Utils.saveRecord(saveButton, silent, successFn, beforeSaveFn, detailView, detailRecordClass);
    }
});
