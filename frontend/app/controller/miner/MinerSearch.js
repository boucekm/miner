Ext.define('Miner.controller.miner.MinerSearch', {
    extend: 'Ext.app.Controller',

    controllers: [
         'miner.MinerList',
         'miner.MinerNew',
         'miner.MinerDetail',  
         'miner.MinerEdit'
    ],

    stores: [
         'miner.MinerListGrid'
    ],
    
    views: [
        'miner.MinerSearch',
        'miner.MinerList'
    ],    
    
    refs: [
        {
            ref: 'minerMinerListRef',
            selector: 'miner-MinerList'
        }
    ],
    
    init: function() {
        this.control({
            'miner-MinerSearch button[action="search"]': {  
                click: this.searchMiner
            },
            'miner-MinerSearch button[action="cancel"]': {  
                click: this.cancelMiner
            }

        });
    },
   
    cancelMiner: function(button) {
        Miner.Utils.mainCardRemoveShowLast();
    },

    searchMiner: function(button) {
        var me = this;
        var view = me.getMinerMinerListRef() || Ext.widget('miner-MinerList');
        var grid = view.down('miner-MinerListGrid');
        
        var wrapper = button.up('main-MainWrapper');
        wrapper.add(view);  
                
        Miner.Utils.searchLoadList(grid, button);
    }
    
});
