Ext.define('Miner.controller.miner.MinerDetail', {
    extend: 'Ext.app.Controller',

    views: [
        'miner.MinerDetail',
        'miner.MinerEdit'
    ],
    
    refs: [
        {
            ref: 'minerMinerEditRef',
            selector: 'miner-MinerEdit'
        }
    ],
    
    init: function() {
        this.control({
            'miner-MinerDetail button[action="edit"]': {  
                click: this.editMinerDetail
            },
            'miner-MinerDetail button[action="cancel"]': {  
                click: this.cancelMinerDetail
            }
        });
    },
   
    cancelMinerDetail: function(button) {
        Miner.Utils.mainCardRemoveShowLast();
    },
    
    editMinerDetail: function(button) {
        var me = this;
        var view = me.getMinerMinerEditRef() || Ext.widget('miner-MinerEdit'); 
        var record = button.up('form').getRecord();
        
        Miner.Utils.mainCardAddShowEdit(view);
        
        view.loadRecord(record);
        view.refreshComponents();
    }
});
