Ext.define('Miner.controller.miner.MinerNew', {
    extend: 'Ext.app.Controller',

    requires: [
        'Miner.model.miner.Miner'    
    ],
    
    views: [
        'miner.MinerNew',
        'miner.MinerDetail'
    ],    
    
    refs: [
        {
            ref: 'minerMinerNewRef',
            selector: 'miner-MinerNew'
        },
        {
            ref: 'minerMinerDetailRef',
            selector: 'miner-MinerDetail'
        }
    ],
    
    init: function() {
        this.control({
            'miner-MinerNew button[action="save"]': {  
                click: this.saveMiner
            },
            'miner-MinerNew button[action="cancel"]': {  
                click: this.cancelMiner
            }
        });
    },
   
    cancelMiner: function(button) {
        Miner.Utils.mainCardRemoveShowLast();
    },
    
    saveMiner: function(saveButton) {
        var me = this;
      
        var successFn = undefined;
        var detailView = me.getMinerMinerDetailRef() || Ext.widget('miner-MinerDetail'); 
        var detailRecordClass = Miner.model.miner.Miner;
        var silent = true;
        var beforeSaveFn = undefined;
        Miner.Utils.saveRecord(saveButton, silent, successFn, beforeSaveFn, detailView, detailRecordClass);
    }
});
