Ext.define('Miner.controller.miner.MinerList', {
    extend: 'Ext.app.Controller',

    requires: [
        'Miner.model.miner.Miner'    
    ],
    
    views: [
        'miner.MinerList',
        'miner.MinerNew',
        'miner.MinerDetail',
        'miner.MinerEdit'
    ],
    
    refs: [
        {   
            ref: 'minerMinerNewRef',                
            selector: 'miner-MinerNew'  
        },
        {
            ref: 'minerMinerDetailRef',
            selector: 'miner-MinerDetail'
        },
        {
            ref: 'minerMinerEditRef',
            selector: 'miner-MinerEdit'
        }
    ],
    
    init: function() {
        this.control({
            'miner-MinerList miner-MinerListGrid dataview': {
                itemdblclick: this.showMinerDetail
            },
            'miner-MinerList button[action="new"]': {  
                click: this.newMiner
            }
        });
    },

    newMiner: function(button) {
        var me = this;
        var searchRecord = button.up('main-MainWrapper').down('miner-MinerSearch').getRecord();
        // updateRecord() is not needed; record is actual because search clicked
        
        var view = me.getMinerMinerNewRef() || Ext.widget('miner-MinerNew');   
        Miner.Utils.mainCardAddShowNew(view);
        
        // fill in fields in edit form from search form   
        var record = Ext.create('Miner.model.miner.Miner');
        record.set('name', searchRecord.get('name'));
        record.set('birth', searchRecord.get('birth'));
        record.set('buried', searchRecord.get('buried'));
        
        view.loadRecord(record);  
        view.refreshComponents();
    },
    
    showMinerDetail: function(view, record, item, index, event, opts) {
        var me = this;       
        var view = me.getMinerMinerDetailRef() || Ext.widget('miner-MinerDetail'); 
        Miner.Utils.mainCardAddShowDetail(view);        
        Miner.Utils.loadRecord(Miner.model.miner.Miner, record.getId(), view);
    }
    
});
