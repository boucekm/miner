Ext.define('Miner.controller.Main', {
    extend: 'Ext.app.Controller',
    
    controllers: [
        'miner.MinerSearch'
    ],

    stores: [
    ],
    
    views: [
        'main.Main',
        'main.MainWrapper',
        'miner.MinerSearch'
    ],
    
    refs: [
        {
            ref: 'minerMinerSearchRef',
            selector: 'miner-MinerSearch'
        }
    ],

    init: function() {
        this.control({
            'main-Main button[action="miner"]': {
                click: this.showMiner
            }
        });
    },
 
    showMiner: function(button) {
        var me = this;
        var view = me.getMinerMinerSearchRef() || Ext.widget('miner-MinerSearch');
        Miner.Utils.mainCardSetComponentFirstShowRemoveRest(view, 'search');
    }
    
});
