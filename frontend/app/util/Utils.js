Ext.define('Miner.util.Utils', {
    singleton: true,
    alternateClassName: 'Miner.Utils',

    requires: [
        'Ext.Msg',      // Ext.MessageBox
        'Miner.view.main.MainWrapper'
    ],

    config: {
    },

    constructor: function(config) {
        var me = this;
        me.initConfig(config);
        return me;
    },
    
    // ----
    
    /**
     * Show modal info dialog.
     * Optional function is called synchronously after dialog closed.
     * @param {string} msg 
     * @param {function} [fn]
     */
    showInfo: function(msg, fn) {
        Ext.Msg.show({
            title: 'Info',
            msg: msg,
            maxWidth: 1600,
            modal: true,
            icon: Ext.Msg.INFO,
            buttons: Ext.Msg.OK,
            fn: (fn ? fn : function() {})
        });
    },
    
    /**
     * Show modal warning dialog.
     * Optional function is called synchronously after dialog closed.
     * @param {string} msg 
     * @param {function} [fn]
     */
    showWarning: function(msg, fn) {  
        Ext.Msg.show({
            title: 'Warning',
            msg: msg,
            maxWidth: 1600,
            modal: true,
            icon: Ext.Msg.WARNING,
            buttons: Ext.Msg.OK,
            fn: (fn ? fn : function() {})
        });
    },
    
    /**
     * Show modal error dialog.
     * Optional function is called synchronously after dialog closed.
     * @param {string} msg 
     * @param {function} [fn]
     */
    showError: function(msg, fn) {
        Ext.Msg.show({
            title: 'Error',
            msg: msg,
            maxWidth: 1600,
            modal: true,
            icon: Ext.Msg.ERROR,
            buttons: Ext.Msg.OK,
            fn: (fn ? fn : function() {})
        });
    },
    
    /**
     * @param {string} msg question message
     * @param {string} defaultButton default focus on button 'yes' or 'no'
     * @param {function} fn callback 
     * 
     * callback is
     * function(buttonId) {
     *   if (buttonId === 'yes') {
     *     ...
     *   }
     */
    showConfirm: function(msg, defaultButton, fn) {
        Ext.Msg.show({
            title: '',
            msg: msg,
            buttons: Ext.Msg.YESNO,
            defaultFocus: defaultButton,
            modal: true,
            icon: Ext.MessageBox.WARNING,
            fn: fn
        });
    },
    
    // ----
     
    createParamsFromRecord: function(record) {
        var me = this;
        var data = {};
        for (var pname in record.getData()) {
            data[pname] = me.createParamFromRecordField(record, pname);
        }
        return data;
    },
    
    createParamFromRecordField: function(record, fname) {
        var result;
        var fvalue = record.getData()[fname];
        if (Ext.isEmpty(fvalue)) {
            result = '';                   // value of request parametr if record field is empty
        } else if (Ext.isDate(fvalue)) {
            result = Ext.Date.format(fvalue, Miner.Config.getDateFormat());
        } else {
            result = fvalue;
        }
        return result;
    },
    
    // ----

    storeSync: function(store, successMsg, successFn) {
        var me = this; 
        
        store.sync({
            success: function() {
                if (successFn) { 
                    successFn.call();
                }
                Miner.Utils.showInfo(successMsg);
            },
            
            failure: function(batch, options) {
                var errShow = false;
                if (batch.total > 0) {
                    for (var idx in batch.operations) {                        
                        errShow = errShow || me.proxyProcessOpException(batch.operations[idx]);  
                    }
                }
                if (!errShow) {    // defaul error message
                    Miner.Utils.showError(Miner.Text['request_communication_error']);
                }
            }
        });
    },
    
    /**
     * Helper function for error processing.
     * 
     * @param {object} operation description
     */
    proxyProcessOpException: function(operation) { 
        var errShow = false;
        if (operation.hasException()) {
            var error = operation.getError();
            if (typeof error === 'string') {
                Miner.Utils.showError(error);
                errShow = true;
            } else if (typeof error === 'object') {
                if (error.status === 0) {
                    Miner.Utils.showError(Miner.Text['request_communication_error']);
                } else {
                    Miner.Utils.showError("Status: " + error.status + ", text: " + error.statusText);
                }
                errShow = true;
            }
        }
        return errShow;
    },
    
    // ----
    
    searchLoadList: function(listView, searchButton) {
        var store = listView.getStore();
        store.loadData([], false);  
        
        var form = searchButton.up('form').getForm();
        if (!form.isValid()) {
            Miner.Utils.showError(Miner.Text['form_validation_error']);
            return;
        }
        
        form.updateRecord();      
        var record = form.getRecord();
        
        store.getProxy().extraParams = Miner.Utils.createParamsFromRecord(record);  
        
        store.loadPage(1, {
            callback: function(records, operation, success) {
                if (!success) {
                    var errShow = Miner.Utils.proxyProcessOpException(operation);
                    if (!errShow) {    // default error message
                        Miner.Utils.showError(Miner.Text['request_communication_error']);
                    }
                    return;
                }
            }
        });
    },
    
    // ----
    
    saveRecordBasic: function(record, successFn, beforeSaveFn) { 
        var me = this;
        var loadingMsgView = Ext.ComponentQuery.query('main-Main #mainCardId')[0];
        
        if (beforeSaveFn) {
            beforeSaveFn.call(me, record);
        }
        
        record.getProxy().extraParams = {};  // cleaning
        
        loadingMsgView.setLoading(Miner.Text['please_wait_text']);
        record.save({
//            params: {'id': record.get('id')},
            callback: function(savedRecord, operation, success) {
                loadingMsgView.setLoading(false);
                if (!success) {
                    var errShow = Miner.Utils.proxyProcessOpException(operation);
                    if (!errShow) {    // default error message
                        Miner.Utils.showError(Miner.Text['request_communication_error']);
                    }                    
                    return;
                }
                
                // savedRecord == record and has changed properties from response
                
                if (successFn) {
                    successFn.call(me, savedRecord);
                }
            }
        });
    },
    
    saveRecord: function(saveButton, silent, successFn, beforeSaveFn, detailView, detailRecordClass) { 
        var me = this;
        var loadingMsgView = Ext.ComponentQuery.query('main-Main #mainCardId')[0];
        var view = saveButton.up('form');
        var form = view.getForm();
        if (!form.isValid()) {
            Miner.Utils.showError(Miner.Text['form_validation_error']);
            return;
        }
        
        form.updateRecord();      
        var record = form.getRecord();
        
        if (beforeSaveFn) {
            beforeSaveFn.call(me, view, record);
        }
        
        record.getProxy().extraParams = {};  // cleaning
        
        loadingMsgView.setLoading(Miner.Text['please_wait_text']);
        record.save({
//            params: {'id': record.get('id')},
            callback: function(savedRecord, operation, success) {
                loadingMsgView.setLoading(false);
                if (!success) {
                    var errShow = Miner.Utils.proxyProcessOpException(operation);
                    if (!errShow) {    // defaul error message
                        Miner.Utils.showError(Miner.Text['request_communication_error']);
                    }                    
                    return;
                }

                // savedRecord == record and has changed properties from response
                view.loadRecord(savedRecord);
                view.refreshComponents();
                if (silent !== true) {
                    Miner.Utils.showInfo('Record has been saved');
                }
                
                if (!successFn) {
                    successFn = function(savedRecord) {
                        console.log('default saveRecord successFn function');
                        var activeItem = Miner.Utils.mainCardGetActiveItem();
                        if (activeItem.viewType === 'new') {  
                            Miner.Utils.mainCardReplaceShow(detailView, 'detail');
                            Miner.Utils.loadRecord(detailRecordClass, savedRecord.getId(), detailView);
                        } else {
                            Miner.Utils.mainCardRemoveShowLast();
                            activeItem = Miner.Utils.mainCardGetActiveItem();
                            if (activeItem.viewType === 'detail') {  
                                detailView = activeItem.items.get(0);  
                                Miner.Utils.loadRecord(detailRecordClass, savedRecord.getId(), detailView);
                            }
                        }
                    };
                }
                successFn.call(me, savedRecord);
            }
        });
    },
    
    loadRecord: function(model, id, view, successFn) {
        var me = this;
        var loadingMsgView = Ext.ComponentQuery.query('main-Main #mainCardId')[0];
        loadingMsgView.setLoading(Miner.Text['please_wait_text']);
        model.load(id,  {
//            params: {'id': id},
            callback: function(loadedRecord, operation, success) {
                loadingMsgView.setLoading(false);
                if (!success) {
                    var errShow = Miner.Utils.proxyProcessOpException(operation);
                    if (!errShow) {    // default error message
                        Miner.Utils.showError(Miner.Text['request_communication_error']);
                    }
                    
                    loadedRecord = model.create();    // the same is Ext.create('model')
//ok                    return;
                }
                
                if (view) {
                    view.loadRecord(loadedRecord);
                    view.refreshComponents();    
                }
                
                if (success && successFn) {
                    successFn.call(me, loadedRecord);
                }
            }
        });
    },
    
    // ----
    
    mainCardGetActiveItem: function() {
        var mainCard = Ext.ComponentQuery.query('main-Main #mainCardId')[0];
        
        var layout = mainCard.getLayout();
        var activeItem = layout.getActiveItem();
//        console.log('activeItem', activeItem);
        return activeItem;
    },

    mainCardGetPreviousItemViewType: function() {
        var mainCard = Ext.ComponentQuery.query('main-Main #mainCardId')[0];

        var numItems = mainCard.items.getCount();
        if (numItems > 1) {
            var previous = mainCard.items.getAt(numItems - 2);
//            console.log('mainCardGetPreviousItemViewType: ' + previous.viewType);
            return previous.viewType;  
        }
        return null;
    },

    mainCardSetComponentFirstShowRemoveRest: function(view, type) {
        var mainCard = Ext.ComponentQuery.query('main-Main #mainCardId')[0];

        var wrapper = Ext.widget('main-MainWrapper');
        wrapper.viewType = type;
        wrapper.add(view);
        
        mainCard.insert(0, wrapper);
        var layout = mainCard.getLayout();
        layout.setActiveItem(0);
        
        var numItems = mainCard.items.getCount();
        for (var i = (numItems - 1); i > 0; i--) {  
            mainCard.remove(i, true);   
//            console.log('remove(' + i + ')');
        }
    },
    
    mainCardAddShow: function(view, type) {
        var mainCard = Ext.ComponentQuery.query('main-Main #mainCardId')[0];
        
        var wrapper = Ext.widget('main-MainWrapper');
        wrapper.viewType = type;
        wrapper.add(view);
        
        mainCard.add(wrapper);
        var layout = mainCard.getLayout();
//        var activeItem = layout.getActiveItem();
//        var index = mainCard.items.indexOf(activeItem);
        var numItems = mainCard.items.getCount();
        layout.setActiveItem(numItems - 1);
//        console.log('numItems after add = ' + numItems);
    },

    mainCardAddShowSearch: function(view) {
        this.mainCardAddShow(view, 'search');
    },
    
    mainCardAddShowList: function(view) {
        this.mainCardAddShow(view, 'list');
    },
    
    mainCardAddShowNew: function(view) {
        this.mainCardAddShow(view, 'new');
    },
    
    mainCardAddShowDetail: function(view) {
        this.mainCardAddShow(view, 'detail');
    },
    
    mainCardAddShowEdit: function(view) {
        this.mainCardAddShow(view, 'edit');
    },
    
    mainCardRemoveShowLast: function(afterShowFn) {
        var me = this;
        var mainCard = Ext.ComponentQuery.query('main-Main #mainCardId')[0];

        var numItems = mainCard.items.getCount();
        if (numItems > 1) {
            var layout = mainCard.getLayout();
            layout.setActiveItem(numItems - 2);
//            console.log('setActiveItem(' + (numItems - 2) + ')');

            if (afterShowFn) {
                afterShowFn.call(me, layout.getActiveItem());
            }
        }
        
//        console.log('numItems before remove = ' + numItems);
        if (numItems > 0) {
            mainCard.remove(numItems - 1, true);    
            numItems = mainCard.items.getCount();
//            console.log('numItems after remove = ' + numItems);
        }
    },
    
    mainCardReplaceShow: function(view, type) {
        var mainCard = Ext.ComponentQuery.query('main-Main #mainCardId')[0];

        var layout = mainCard.getLayout();
        var activeItem = layout.getActiveItem();
        var idx = mainCard.items.indexOf(activeItem);
//        console.log('active item idx = ' + idx);

        var wrapper = Ext.widget('main-MainWrapper');
        wrapper.viewType = type;
        wrapper.add(view);
        
        mainCard.insert(idx, wrapper);
        layout.setActiveItem(idx);
        mainCard.remove(idx + 1, true);    
//        console.log('remove(' + (idx + 1) + ')');
    },
    
    mainCardShowIdxRemoveRest: function(idx) {
        var mainCard = Ext.ComponentQuery.query('main-Main #mainCardId')[0];

        var layout = mainCard.getLayout();
        layout.setActiveItem(idx);
        
        var numItems = mainCard.items.getCount();
        for (var i = (numItems - 1); i > idx; i--) {
            mainCard.remove(i, true);   
//            console.log('remove(' + i + ')');
        }
    },
    
    mainCardShowComponentRemoveRest: function(component) {
        var mainCard = Ext.ComponentQuery.query('main-Main #mainCardId')[0];

        var idx = mainCard.items.indexOf(component);
        var layout = mainCard.getLayout();
        layout.setActiveItem(idx);
        
        var numItems = mainCard.items.getCount();
        for (var i = (numItems - 1); i > idx; i--) {
            mainCard.remove(i, true);   
//            console.log('remove(' + i + ')');
        }
    }
    
});
