Ext.define('Miner.util.Config', {
    singleton: true,
    alternateClassName: 'Miner.Config',

    config: {
        
        pageWidth: 600,
        
        formFieldWidth: 300,   
        
        // format for type Date; used for creating of GET parameter date in request
        dateFormat: 'Y-m-d\\TH:i:s.uO',
        
        // number of rows in grids
        storePageSize: 15, 
        
        // server communication timeout
        proxyTimeout: 30000,

        // URL prefix for all requests
        proxyUrlPrefix: 'http://localhost:8080'
//        proxyUrlPrefix: '/backend'
    },
    
    
    getPageWidth: function() {
        return this.config.pageWidth;
    },
    getFormFieldWidth: function() {
        return this.config.formFieldWidth;
    },
    
    
    constructor: function(config) {
        var me = this;
        me.initConfig(config);
        
        // localization for form field xtype 'datefield'
        // it is required to specify alternative formats too
        Ext.define('Miner.locale.cs.form.field.Date', {
            override: 'Ext.form.field.Date',
            format: 'j.n.Y',                        // format for Ext.Date; 1.1.2014
            altFormats: 'd.m.Y|j.m.Y|d.n.Y|Y-m-d'   // some alternative formats
        }); 

        // localization for model field type 'date'
        Ext.define('Miner.locale.cs.data.Field', {
            override: 'Ext.data.Field',
            dateReadFormat: 'Y-m-d\\TH:i:s.uO',     // "2014-02-25T20:16:38.533+0100"
            dateWriteFormat: 'Y-m-d\\TH:i:s.uO',
            dateFormat: 'Y-m-d\\TH:i:s.uO'
        }); 

        // localization for grid column
        Ext.define('Miner.locale.cs.grid.column.Date', {
            override: 'Ext.grid.column.Date',
            format: 'j.n.Y'
        });
        
        // component for displaying date fields
        Ext.define('Miner.form.field.DateDisplayField', {
             extend: 'Ext.form.field.Display',
             alias: 'widget.datedisplayfield',

             datePattern: 'j.n.Y',

             valueToRaw: function(value) {
                 return Ext.util.Format.date(value, this.datePattern);
             }
        });

        // component for displaying boolean fields
        Ext.define('Miner.form.field.CheckboxDisplayField', {
             extend: 'Ext.form.field.Display',
             alias: 'widget.checkboxdisplayfield',

             valueToRaw: function(value) {
                 return (value === true ? 'yes' : 'no');
             }
        });
        
        // redefine behavior of all mandatory form fields
        Ext.define('Ext.form.field.BaseOverride', {
            override: 'Ext.form.field.Base',
            
            // align and width for all the form fields
            labelAlign: 'right', 
            width: me.formFieldWidth,
            
            initComponent: function() {
                var me = this;
                if (me.allowBlank === false && me.fieldLabel) {   
                    me.beforeLabelTextTpl = '*';
//                    me.afterLabelTextTpl = '*';
                    me.labelClsExtra = 'miner-field-required';      // mandatory fields will be bold style
                }
                
                me.callParent(arguments);
            }
        });

        return me;
    }

});
