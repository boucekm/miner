Ext.define('Miner.util.Text', {
    singleton: true,
    alternateClassName: 'Miner.Text',

    requires: [
    ],

    config: {
    },

    constructor: function(config) {
        var me = this;
        me.initConfig(config);
        
        return me;
    },
    
    
    request_communication_error: 'Communication error!',

    please_wait_text: 'Please wait...',
    
    form_validation_error: 'Invalid form data!'
    
});
