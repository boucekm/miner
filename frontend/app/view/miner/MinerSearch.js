Ext.define('Miner.view.miner.MinerSearch', {
    extend: 'Ext.form.Panel',

    requires: [
        'Ext.layout.container.Anchor',
        'Miner.view.miner.MinerSearchContainer',
        'Miner.model.miner.MinerSearch'
    ],

    alias: 'widget.miner-MinerSearch',

    title: 'Miner search',
    layout: 'anchor',
    defaults: {anchor: '100%'},
    bodyPadding: 5,
    
    items: [
        {
            xtype: 'miner-MinerSearchContainer'
        }
    ],
    
    buttonAlign: 'right',  
    buttons: [
        {
            text: 'Clear',
            handler: function() {
                this.up('form').getForm().reset();
            }
        },
        {
            text: 'Search',
            action: 'search' 
        },
        {
            text: 'Cancel',
            action: 'cancel'
        }
    ],
    
    initComponent: function() {
        var me = this;
        me.callParent(arguments);

        me.loadRecord(Ext.create('Miner.model.miner.MinerSearch'));    
    },
    
    refreshComponents: function() {    
        var me = this;        
        var record = me.getRecord();
       
        me.getForm().clearInvalid();
    }
});
