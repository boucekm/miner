Ext.define('Miner.view.miner.MinerNew', {
    extend: 'Ext.form.Panel',

    requires: [
        'Ext.layout.container.Anchor',
        'Miner.view.miner.MinerNewContainer', 
        'Miner.model.miner.Miner'
    ],

    alias: 'widget.miner-MinerNew',

    title: 'New miner',  
    layout: 'anchor',
    defaults: {anchor: '100%'},
    bodyPadding: 5,
    
    items: [
        {
            xtype: 'miner-MinerNewContainer'
        }
    ],
    
    buttonAlign: 'right',  
    buttons: [
        {
            text: 'Save',
            action: 'save'
        },
        {
            text: 'Cancel',
            action: 'cancel'
        }
    ],
    
    refreshComponents: function() {    
        var me = this;        
        var record = me.getRecord();
       
        me.getForm().clearInvalid();
    }
});
