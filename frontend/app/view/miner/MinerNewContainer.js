Ext.define('Miner.view.miner.MinerNewContainer', {
    extend: 'Ext.container.Container',

    requires: [
        'Ext.layout.container.Anchor'
    ],
    
    alias: 'widget.miner-MinerNewContainer',

    layout: 'anchor',
    items: [
        {
            xtype: 'textfield',
            name : 'name',
            fieldLabel: 'Name',
            allowBlank: false,   
            allowOnlyWhitespace: false,
            blankText: 'Miner name is mandatory field',
            enforceMaxLength: true,
            maxLength: 255
        },
        {
            xtype: 'datefield',
            name : 'birth',
            fieldLabel: 'Date of birth',
            allowBlank: false,   
            allowOnlyWhitespace: false,
            blankText: 'Date of birth is mandatory field'
        },
        {
            xtype: 'checkboxfield',
            name : 'buried',
            fieldLabel: 'Buried'
        }
    ]
});
