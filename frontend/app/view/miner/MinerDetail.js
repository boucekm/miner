Ext.define('Miner.view.miner.MinerDetail', {
    extend: 'Ext.form.Panel',

    requires: [
        'Ext.layout.container.Anchor',
        'Miner.view.miner.MinerDetailContainer', 
        'Miner.model.miner.Miner'
    ],

    alias: 'widget.miner-MinerDetail',

    title: 'Miner detail',  
    layout: 'anchor',
    defaults: {anchor: '100%'},
    bodyPadding: 5,
    
    items: [
        {
            xtype: 'miner-MinerDetailContainer'
        }
    ],
    
    buttonAlign: 'right',  
    buttons: [
        {
            text: 'Edit',
            action: 'edit'
        },
        {
            text: 'Back',
            action: 'cancel'
        }
    ],
    
    refreshComponents: function() {  
        var me = this;        
        var record = me.getRecord();
       
        me.getForm().clearInvalid();
    }
});
