Ext.define('Miner.view.miner.MinerDetailContainer', {
    extend: 'Ext.container.Container',

    requires: [
        'Ext.layout.container.Anchor'
    ],
    
    alias: 'widget.miner-MinerDetailContainer',

    layout: 'anchor',
    items: [
        {
            xtype: 'displayfield',
            name : 'name',
            fieldLabel: 'Name'
        },
        {
            xtype: 'datedisplayfield',
            name : 'birth',
            fieldLabel: 'Date of birth'
        },
        {
            xtype: 'checkboxdisplayfield',
            name : 'buried',
            fieldLabel: 'Buried'
        }
    ]
});
