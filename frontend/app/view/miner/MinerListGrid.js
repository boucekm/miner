Ext.define('Miner.view.miner.MinerListGrid', {
    extend: 'Ext.grid.Panel',

    requires: [
        'Ext.toolbar.Paging',
        'Ext.grid.column.Boolean',
        'Ext.grid.column.Date'
    ],
    
    alias: 'widget.miner-MinerListGrid',
    title: 'Miner search result',    
    store: 'miner.MinerListGrid',
    
    columns: [
        { 
            text: 'Name', 
            dataIndex: 'name',
            flex: 1,
            sortable: true,
            hideable: true
        },
        {
            text: 'Birth', 
            dataIndex: 'birth',
            xtype:'datecolumn',
            flex: 0.5,
            sortable: true,
            hideable: true
        },
        {
            text: 'Buried', 
            dataIndex: 'buried',
            xtype: 'booleancolumn', 
            trueText: 'yes',
            falseText: 'no', 
            flex: 0.2,
            sortable: true,
            hideable: true
        }
    ],
    
    dockedItems: [
        {
            xtype: 'pagingtoolbar',
            store: 'miner.MinerListGrid',
            dock: 'bottom',
            displayInfo: true
        }
    ]
    
});
