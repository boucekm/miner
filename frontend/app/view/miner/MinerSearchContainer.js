Ext.define('Miner.view.miner.MinerSearchContainer', {
    extend: 'Ext.container.Container',

    requires: [
        'Ext.layout.container.Anchor',
        'Ext.form.field.Date',
        'Ext.form.field.Checkbox'
    ],

    alias: 'widget.miner-MinerSearchContainer',

    layout: 'anchor',
    items: [
        {
            xtype: 'textfield',
            name : 'name',
            fieldLabel: 'Name'
        },
        {
            xtype: 'datefield',
            name : 'birth',
            fieldLabel: 'Date of birth'
        },
        {
            xtype: 'checkboxfield',
            name : 'buried',
            fieldLabel: 'Buried'
        }
    ]
});
