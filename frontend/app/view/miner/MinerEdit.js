Ext.define('Miner.view.miner.MinerEdit', {
    extend: 'Ext.form.Panel',

    requires: [
        'Ext.layout.container.Anchor',
        'Miner.view.miner.MinerEditContainer', 
        'Miner.model.miner.Miner'
    ],

    alias: 'widget.miner-MinerEdit',

    title: 'Miner edit',  
    layout: 'anchor',
    defaults: {anchor: '100%'},
    bodyPadding: 5,
    
    items: [
        {
            xtype: 'miner-MinerEditContainer'
        }
    ],
    
    buttonAlign: 'right',  
    buttons: [
        {
            text: 'Save',
            action: 'save'
        },
        {
            text: 'Cancel',
            action: 'cancel'
        }
    ],
    
    refreshComponents: function() {  
        var me = this;        
        var record = me.getRecord();
       
        me.getForm().clearInvalid();
    }
});
