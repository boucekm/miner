Ext.define('Miner.view.miner.MinerList', {
    extend: 'Ext.container.Container',

    requires: [
        'Ext.layout.container.Anchor',
        'Ext.layout.container.HBox',
        'Ext.button.Button',
        'Miner.view.miner.MinerListGrid'
    ],
    
    alias: 'widget.miner-MinerList',
    
    layout: 'anchor',
    width: Miner.Config.getPageWidth(),
    items: [
        {
            xtype: 'miner-MinerListGrid'
        },
        {
            layout: {
                type: 'hbox',
                pack: 'end',
                align: 'middle',
                padding: '5'
//                defaultMargins: '0 0 0 10'      
            },
            margin: '5 0 0 0',
            ui: 'footer',
            items: [
                {
                    xtype: 'button',
                    text: 'New miner',
                    action: 'new'
                }
            ]
        }
    ],
    
    refreshComponents: function() {  
        var me = this;        
       
    }
});
