Ext.define('Miner.view.Viewport', {
    extend: 'Ext.container.Viewport',
    
    requires: [
        'Miner.view.main.Main'
    ],
    
    layout: 'fit',
    items: {
        xtype: 'main-Main'
    }
});
