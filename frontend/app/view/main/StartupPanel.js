Ext.define('Miner.view.main.StartupPanel', {
    extend: 'Ext.container.Container',

    requires: [
        'Ext.layout.container.Anchor',
        'Ext.form.Label'
    ],

    alias: 'widget.main-StartupPanel',

    title: '',  
    layout: 'anchor',
    defaults: {anchor: '100%'},
    bodyPadding: 5,
    items: [
        {
            xtype: 'label',
            itemId: 'textId',
            tesx: ''
        }
    ],
    
    initComponent: function() {
        var me = this;
        me.callParent(arguments);
        
        var label1 = me.down('#textId');
        var text = '<p><b>Miner</b> (demo for Spring Boot presentation)';
        label1.html = text;
    }
    
});
