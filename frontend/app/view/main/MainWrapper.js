/**
 * Container wrapper for any view added into main card layout.
 * Items are added manually.
 */
Ext.define('Miner.view.main.MainWrapper', {
    extend: 'Ext.container.Container',

    requires: [
        'Ext.layout.container.Anchor',
        'Miner.util.Config'
    ],

    alias: 'widget.main-MainWrapper',
    
    layout: 'anchor',
    defaults: {
        padding: '10 8 0 3',
        width: Miner.Config.getFormFieldWidth() + 50
//        width: Miner.Config.getPageWidth() 
    },
    items: [],
    
    
    /**
     * Type of view that is in this wrapper.
     * Helper functions.
     * Typical values are "search", "list", "new", "detail", "edit".
     */
    viewType: undefined,         
    
    isViewTypeSearch: function() {
        return (this.viewType === 'search');
    },
    
    isViewTypeList: function() {
        return (this.viewType === 'list');
    },
    
    isViewTypeNew: function() {
        return (this.viewType === 'new');
    },
    
    isViewTypeDetail: function() {
        return (this.viewType === 'detail');
    },
    
    isViewTypeEdit: function() {
        return (this.viewType === 'edit'); 
    }
});
