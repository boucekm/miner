Ext.define('Miner.view.main.Main', {
    extend: 'Ext.container.Container',

    requires: [
        'Ext.layout.container.Border',
        'Ext.layout.container.Column',
        'Ext.layout.container.Anchor',
        'Ext.layout.container.Card',
        'Miner.util.Config',
        'Miner.view.main.StartupWrapper'
    ],

    alias: 'widget.main-Main',
    
    layout: 'border',
    items: [
        {
            region: 'west',
            xtype: 'panel',
            title: 'Menu',
            bodyPadding: 5,
            
            collapsible: true,    
            collapsed: true,    
            collapseMode: 'header',
            split: true,           
            splitterResize: true,  
            
            layout: 'anchor',
            defaults: {anchor: '100%'},
            
            items: {
                xtype: 'container',
                layout: {
                    type: 'vbox',
                    align: 'stretch', 
                    pack: 'start'    
                },
                
                defaults: { 
                    xtype: 'button', 
                    margin: '0 0 5 0' 
                },
                items: [
                    {
                        text: 'Miner',
                        action: 'miner',
                        width: 70
                    }
                ]
            }
        },
        {
            region: 'center',
            xtype: 'container',
            autoScroll: true,
            layout: {
                type: 'vbox',
//                align: 'stretch', 
                align: 'left', 
                pack: 'start'    
            },
            defaults: { 
                width: Miner.Config.getPageWidth() 
            },
            items: [
                {
                    xtype: 'container',
                    layout: 'card',
//                    activeItem: 0,
                    itemId: 'mainCardId',
                    items: [    // main wrapper containers
                        {
                            xtype: 'main-StartupWrapper'   // start panel
                        }
                    ]   
                }
            ]
        }
    ]
});
