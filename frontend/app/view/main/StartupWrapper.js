Ext.define('Miner.view.main.StartupWrapper', {
    extend: 'Miner.view.main.MainWrapper',

    requires: [
        'Ext.layout.container.Anchor',
        'Miner.view.main.StartupPanel'
    ],

    alias: 'widget.main-StartupWrapper',

    title: '',
    layout: 'anchor',
    defaults: {anchor: '100%'},
    bodyPadding: 5,
    items: [
        {
            xtype: 'main-StartupPanel',
            margin: '40 0 0 40'
        }
    ]
});
