Ext.define('Miner.store.miner.MinerListGrid', {
    extend: 'Ext.data.Store',

    requires: [
        'Ext.data.proxy.Rest'
    ],
    
    pageSize: Miner.Config.getStorePageSize(),
    autoLoad: false,
    model: 'Miner.model.miner.Miner',
    
    remoteSort: false,
    buffered: false,
    
    proxy: {
        type: 'rest',
        url: Miner.Config.getProxyUrlPrefix() + '/miner',
        appendId: true,
        noCache: true,
        cacheString: '_dc',
        pageParam: 'page',
        startParam: 'start',
        limitParam: 'limit',
        timeout: Miner.Config.getProxyTimeout(),
        idParam: 'id',
        
        simpleSortMode: true,  
        sortParam: 'sort',
        directionParam: 'dir', 

        reader: {
            type: 'json',
            root: 'records',
            idProperty: 'id',
            totalProperty: 'total',
            successProperty: 'success',
            messageProperty: 'message'
        }
    }
});
