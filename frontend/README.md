# README #

This is a frontend for demo Spring Boot presentation.

### Prerequisites ###

* ExtJS SDK (ext-4.2.1)
* Sencha cmd

### How do I get set up? ###

* Initialization of directory miner/frontend/ and ExtJS workspace in it:

miner/> sencha -sdk c:\tmp\ext-4.2.1.883-SDK\  generate app Miner frontend

* Build application with: 

miner/frontend/> sencha app build

